Interact with data!

Digital world is fantastic but when presenting to a wider public, physical interaction is crucial. While everything can be seen on a computer, you will never forget your last visit to Questacon. Museums, schools, environment agencies can all have such feature in a lobby or as an exhibit. Super low power consumption and standard components make this easy to maintain, update or even fix.

Kids or adults (including childish adults!) still like to press a button. Well, we have two!

InfoBox v1 is just that. We have much more advanced concept but would need more time to develop it (investors - wink-wink!). Ideas such as gesture scrolling through data (easy!) or online data feeder so you do not need to . We took some critical data describing Australia and some if it just may surprise you.

There are only two buttons to use! Use one to select a state vs Australia value and select another one to select topic.

Device will generate simple web output with a simple Ethernet connection that can be embedded in your website. As a proof of concept, we created a website that embeds it and can be opened on any laptop or in our case - a Raspberry PI. This presents a fully contained system: interface-website-TV with no external dependencies.

But if you are creative, you can put it online and connect a device on the other side of the worlds - just expose port 80 from InfoBox and use our website or make your own to embed it.

Cost to produce is very low.

v2 plans include (proof of concepts already partially developed for some):

- download on boot to get fresh data (or save to SD and download on request)
- ultrasonic (demo!) or infrared distance sensor to scroll through data or datasets
- multiple years, not just 2016
- unified platform (all on Raspberri PI) so it is all in one device instead of device plus laptop or RPI as in our case
- WiFi instead of Ethernet (especially if Raspberry PI)